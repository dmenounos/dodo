mojo.xml = {

	Object: mojo.extend(mojo.Object),

	create: function(type) {
		return mojo.extend(mojo.xml.Object, {
			_type: type,
			toXml: function() {
				return mojo.xml.convert(this);
			}
		});
	},

	convert: function(obj, node) {
		var text = '';

		if (isPrimitive(obj)) {
			var type, typeName;

			switch (type = primitiveType(obj)) {
				case 1: case 2: typeName = 'boolean'; break;
				case 3: case 4: typeName = 'int'; break;
				case 5: case 6: typeName = 'string'; break;
			}

			if (!node) {
				// console.log('stand-alone primitive: ' + obj);
				text = '<' + typeName + '>' + obj + '</' + typeName + '>';
			}
			else if (type % 2 == 1) {
				// console.log('hosted scalar primitive: ' + node + ' ' + obj);
				text = '<' + node + '>' + obj + '</' + node + '>';
			}
			else if (type % 2 == 0) {
				// console.log('hosted object primitive: ' + node + ' ' + obj);
				text = '<' + node + ' class="' + typeName + '">' + obj + '</' + node + '>';
			}
		}
		else if (isArray(obj)) {
			for (var x = 0; x < obj.length; x++) {
				text += this.convert(obj[x]);
			}

			if (!node) {
				// console.log('stand-alone array-list');
				text = '<list>' + text + '</list>';
			}
			else {
				// console.log('hosted array-list: ' + node);
				text = '<' + node + '>' + text + '</' + node + '>';
			}
		}
		else if (isObject(obj) && obj instanceof mojo.xml.Object) {
			for (var prop in obj) {
				if (prop == '_type') continue;
				text += this.convert(obj[prop], prop);
			}

			if (!node) {
				// console.log('stand-alone object: ' + obj._type);
				text = '<' + obj._type + '>' + text + '</' + obj._type + '>';
			}
			else {
				// console.log('hosted object: ' + node + ' ' + obj._type);
				text = '<' + node + ' class="' + obj._type + '">' + text + '</' + node + '>';
			}
		}

		return text;
	}
};
