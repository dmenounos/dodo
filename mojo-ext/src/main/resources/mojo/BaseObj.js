mojo = {

	Object: function(cfg) {
		applyObj(this, cfg);
	},

	extend: function(sup, def) {
		var sub = function() {
			sup.apply(this, arguments);
		};

		sub.prototype = new sup();
		applyObj(sub.prototype, def);
		return sub;
	}
};
