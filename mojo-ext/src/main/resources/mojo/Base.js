function isNil(obj) {
       return obj == null || typeof obj == 'undefined';
}

function isObject(obj) {
       return !isNil(obj) && typeof obj == 'object';
}

function isScalar(obj) {
       return !isNil(obj) && typeof obj != 'object' && typeof obj != 'function';
}

function isPrimitive(obj) {
       return isString(obj) || isNumber(obj) || this.isBoolean(obj);
}

function isBoolean(obj) {
       return typeof obj == 'boolean' || obj instanceof Boolean;
}

function isNumber(obj) {
       return typeof obj == 'number' || obj instanceof Number;
}

function isString(obj) {
       return typeof obj == 'string' || obj instanceof String;
}

function isArray(obj) {
       return obj instanceof Array;
}

function primitiveType(obj) {
	if (isScalar(obj)) {
		if (typeof obj == 'boolean') {
			return 1;
		}

		if (typeof obj == 'number') {
			return 3;
		}

		if (typeof obj == 'string') {
			return 5;
		}
	}

	if (isObject(obj)) {
		if (obj instanceof Boolean) {
			return 2;
		}

		if (obj instanceof Number) {
			return 4;
		}

		if (obj instanceof String) {
			return 6;
		}
	}

	return 0;
}

function applyObj(to) {
	if (!isObject(to)) to = {};

	for (var i = 1; i < arguments.length; i++) {
		var from = arguments[i];

		if (isObject(from)) {
			for (var p in from) {
				to[p] = from[p];
			}
		}
	}

	return to;
}

function cloneObj(obj) {
	if (obj == null || typeof obj != 'object') {
		return obj;
	}

	if (obj instanceof String) {
		return new String(obj.valueOf());
	}

	if (obj instanceof Number) {
		return new Number(obj.valueOf());
	}

	if (obj instanceof Boolean) {
		return new Boolean(obj.valueOf());
	}

	if (obj instanceof Array) {
		var temp = [];

		for (var i = 0; i < obj.length; i++) {
			temp.push(mojo.cloneObj(obj[i]));
		}

		return temp;
	}

	var result = {};

	for (var key in obj) {
		result[key] = mojo.cloneObj(obj[key]);
	}

	return result;
}
