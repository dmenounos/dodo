Ext.namespace('mojo');

mojo.EntitySubCombo = Ext.extend(mojo.EntityCombo, {

	linkProperty: 'masterId',

	initComponent: function() {
		this.parentCombo.on('change', function() {
			if (!this.supportMode) {
				this.clearValue();
			}

			this.lastQuery = null;
		}, this);

		mojo.EntitySubCombo.superclass.initComponent.call(this);
	},

	initStore: function() {
		this.store.comp = this;
		this.filterType = this.store.filterType;
		this.selectType = this.store.selectDetailType;
	},

	getMasterId: function() {
		var parentValue = this.parentCombo.getValue();
		return parentValue ? new Number(parentValue.id) : null;
	},

	selectQuery: function() {
		var query = mojo.EntitySubCombo.superclass.selectQuery.call(this);
		query[this.linkProperty] = this.getMasterId();
		return query;
	}
});

Ext.ComponentMgr.registerType('entitysubcombo', mojo.EntitySubCombo);
