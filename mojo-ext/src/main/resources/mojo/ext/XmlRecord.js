mojo.record = {

	states: {
		NORMAL: 0,
		CREATE: 1,
		UPDATE: 2,
		DELETE: 3
	},

	create: function(fields, type) {

		// add state field
		fields.splice(0, 0, {
			name: '_state', type: 'int',
			defaultValue: mojo.record.states.NORMAL
		});

		// xml types
		var types = {};

		// create xml object type
		types[type] = mojo.xml.create(type);

		// enhance it's prototype - required for 
		// EntityCombo grid editor functionality
		types[type].prototype.toString = function() {
			return String(this.id); // id is number
		};

		// create field xml object converters
		for (var x = 0; x < fields.length; x++) {
			var field = fields[x];

			if (typeof field.convert == 'string') {
				var subtype = field.convert;

				// create field xml object type
				types[subtype] = mojo.xml.create(subtype);
				types[subtype].prototype.toString = function() {
					return this.id; // id is string
				};

				field.convert = this.getFieldConverter(field.name, types[subtype]);
			}
		}

		// create plain ext record type
		var rectype = Ext.data.Record.create(fields);

		// enhance it's prototype - required for 
		// EntityGrid CRUD functionality
		Ext.apply(rectype.prototype, {
			getState : this.getState,
			setState : this.setState,
			getData  : this.getData,
			setData  : this.setData,
			getType  : this.getType,
			_types   : types,
			_type    : type
		});

		return rectype;
	},

	getState: function() {
		return this.get('_state');
	},

	setState: function(state) {
		this.set('_state', state);
	},

	getType: function(type) {
		return this._types[type || this._type];
	},

	getData: function() {
		var type = this.getType();
		var obj = new type();

		this.fields.each(function(item, index, length) {
			if (item.name == '_state') return;
			var value = this.get(item.name);

			if (item.type == Ext.data.Types.DATE) {
				value = value // yyyy-MM-dd HH:mm:ss.S z
				      ? value.format('Y-m-d H:i:s.u') + ' GMT' + value.format('P')
				      : null;
			}

			obj[item.name] = value;
		}, this);

		return obj;
	},

	setData: function(obj) {
		this.fields.each(function(item, index, length) {
			if (item.name == '_state') return;
			this.set(item.name, obj[item.name]);
		}, this);
	},

	// private
	getFieldConverter: function(field, type) {
		var self = this;
		return function(value, row) {
			try {
				return self.convertField(row, field, type);
			}
			catch (e) {
				console.log('Failed to extract field: ' + field);
			}

			return null;
		};
	},

	// private
	convertField: function(row, field, type) {
		var root = Ext.DomQuery.selectNode(field, row);

		if (root) {
			var path = root.getAttribute('reference');

			if (path) {
				// xstream reference node
				root = this.resolveReference(root, path);
			}

			if (root.childNodes.length == 0) {
				// hibernate uninitialized proxy node
				return null;
			}
		}
		else {
			// normally should not happen
			// maybe throw an exception ?
			return null;
		}

		var obj = new type();

		for (var x = 0; x < root.childNodes.length; x++) {
			var node = root.childNodes[x];

			if (node.childNodes.length == 1 && node.firstChild.nodeType == 3) {
				obj[node.nodeName] = node.firstChild.nodeValue;
				// values will be strings ...
			}
		}

		return obj;
	},

	// private
	resolveReference: function(node, path) {
		// console.log('resolving reference node: ' + path);
		var parts = path.split('/');

		for (var x = 0; x < parts.length; x++) {
			var part = parts[x];

			if (part == '..') {
				// console.log('moving up');
				node = node.parentNode;
			}
			else {
				// console.log('moving down');
				node = this.findChild(node, part);
			}
		}

		return node;
	},

	// private
	findChild: function(parent, childName) {
		// console.log('searching child node: ' + childName);

		var childPos = 1;
		var childPosIdx = childName.indexOf('[');

		if (childPosIdx != -1) {
			childPos = parseInt(childName.substring(childPosIdx + 1, childName.length - 1));
			childName = childName.substring(0, childPosIdx);
		}

		for (var x = 0; x < parent.childNodes.length; x++) {
			var child = parent.childNodes[x];

			if (child.nodeName == childName && --childPos == 0) {
				return child;
			}
		}

		return null;
	}
};
