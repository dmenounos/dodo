Ext.namespace('mojo');

mojo.FilterPlugin = Ext.extend(Object, {

	msgSearch: 'Search',
	msgApply: 'Apply',

	constructor: function(config) {
		Ext.apply(this, config);
	},

	init: function(grid) {
		this.grid = grid;
		this.grid.selectFilter = this.selectFilter.createDelegate(this);

		this.searchField = new Ext.form.TextField();

		var searchItems = [];
		var columns = grid.colModel.config;

		for (var x = 0; x < columns.length; x++) {
			var column = columns[x];

			if (column.search) {
				searchItems.push(new Ext.menu.CheckItem({
					text: column.header || column.dataIndex,
					value: grid.dataPaths[column.dataIndex],
					hideOnClick: false
				}));
			}
		}

		this.searchButton = new Ext.Toolbar.SplitButton({
			text: this.msgApply,
			menu: new Ext.menu.Menu({
				items: searchItems
			}),
			handler: function(button) {
				this.grid.store.on('load', function() {
					this.searchButton.enable();
				}, this, { signle: true });

				this.searchButton.disable();

				this.grid.store.load({
					params: {
						start: 0,
						limit: this.grid.pageSize
					}
				});
			},
			scope: this
		});

		var tidx = grid.readOnly ? 0 : 3;
		var tbar = grid.topToolbar;

		if (!tbar) {
			tbar = grid.topToolbar = new Ext.Toolbar();
			grid.elements += ',tbar';
		}

		tbar.insert(tidx, new Ext.Button({
			text: this.msgSearch,
			iconCls: 'o-eg-search',
			enableToggle: true,
			toggleHandler: function(btn, pressed) {
				if (pressed) {
					if (!this.searchPanel) {
						var pos = btn.el.getXY();
						var height = btn.el.getHeight();

						this.searchPanel = new Ext.Panel({
							x: pos[0],
							y: pos[1] + height,
							renderTo: document.body,
							style: 'position: absolute; z-index: 1',
							bodyStyle: 'padding: 4px',
							layout: 'table',
							layoutConfig: { columns: 2 },
							items: [ this.searchField, this.searchButton ]
						});

						this.grid.on('destroy', function() {
							this.searchPanel.destroy();
						}, this);
					} else {
						this.searchPanel.show();
					}
				} else {
					this.searchPanel.hide();
				}
			},
			scope: this
		}));
	},

	selectFilter: function() {
		var searchValue = this.searchField.getValue();

		if (searchValue) {
			var searchProperties = [];

			for (var x = 0; x < this.searchButton.menu.items.length; x++) {
				var item = this.searchButton.menu.items.itemAt(x);

				if (item.checked) {
					searchProperties.push(item.value);
				}
			}

			if (searchProperties.length > 0) {
				return Ext.apply(new this.grid.filterType(), {
					value: searchValue,
					properties: searchProperties
				});
			}
		}

		return null;
	}
});
