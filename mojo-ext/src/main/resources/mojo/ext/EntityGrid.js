Ext.namespace('mojo');

mojo.EntityGrid = Ext.extend(Ext.grid.EditorGridPanel, {

	msgAdd: 'New',
	msgRemove: 'Delete',
	msgSubmit: 'Submit',

	pageSize: 100,
	loadMask: true,

	filter: true,

	initComponent: function() {
		this.idcnt = 0;
		this.sm = new Ext.grid.RowSelectionModel();
		this.initStore();

		this.store.on('load', function(store, records, options) {
			this.submitAction.disable();
		}, this);

		this.store.on('update', function(store, record, operation) {
			if (operation == Ext.data.Record.EDIT) {
				if (record.getState() == mojo.record.states.NORMAL) {
					record.setState(mojo.record.states.UPDATE);
				}

				this.submitAction.enable();
			}
		}, this);

		this.store.on('beforeload', function(store, options) {
			var params = options.params;

			// The params.sort property contains the record field on which we 
			// need to sort. This may not map to the actual entity property.

			// For example a 'magazine' record having a 'publisher' field. 
			// It could be that the 'publisher' property is an object type 
			// instead of plain string.

			if (params.sort) {
				params.order = this.dataPaths[params.sort] + ' ' + params.dir;
			}
		}, this);

		// Init data-paths and default renderers...
		// Data-Paths differ to Data-Indexes in that they are complete.

		// Using the above example (magazine record with publisher field), 
		// the dataIndex would be 'publisher' while the dataPath['publisher'] 
		// would be 'publisher.name'

		this.dataPaths = {};

		for (var x = 0; x < this.columns.length; x++) {
			var column = this.columns[x];
			var dataPath = column.dataIndex;
			var dataIndex = column.dataIndex;
			var renderer = column.renderer;
			var editor = column.editor;
			var search = column.search;

			if (editor instanceof mojo.EntityCombo && !renderer) {
				renderer = column.renderer = this.objRenderer(editor.displayField);
			}

			if (renderer && renderer.path) {
				dataPath += '.' + renderer.path;
			}
			else if (typeof search == 'string') {
				dataPath = search;
			}

			this.dataPaths[dataIndex] = dataPath;
		}

		// Init default columns

		this.columns.splice(0, 0, { dataIndex: 'id', header: '#', width: 40, hidden: true, renderer: this.idRenderer });
		this.columns.splice(0, 0, { header: '', width: 40, renderer: this.stateRenderer });

		// Init default actions

		this.addAction = new Ext.Action({
			text: this.msgAdd,
			iconCls: 'o-eg-add',
			handler: this.addHandler,
			scope: this
		});

		this.removeAction = new Ext.Action({
			text: this.msgRemove,
			iconCls: 'o-eg-remove',
			handler: this.removeHandler,
			scope: this
		});

		this.submitAction = new Ext.Action({
			disabled: true,
			text: this.msgSubmit,
			iconCls: 'o-eg-submit',
			handler: this.submitHandler,
			scope: this
		});

		if (!this.readOnly) {
			var insert = function(bar, pos, obj) {
				if (bar instanceof Ext.Toolbar) {
					bar.insert(pos, obj);
				}
				else if (bar instanceof Array) {
					bar.splice(pos, 0, obj);
				}
			};

			if (!this.tbar) {
				this.tbar = new Ext.Toolbar();
			}

			insert(this.tbar, 0, this.addAction);
			insert(this.tbar, 1, this.removeAction);
			insert(this.tbar, 2, this.submitAction);
		}
		else {
			this.on('beforeedit', function(event) {
				event.cancel = true;
			});
		}

		// Init filter plugin

		if (this.filter) {
			var filterPlugin =  new mojo.FilterPlugin();

			if (!this.plugins) {
				this.plugins = filterPlugin;
			}
			else if (this.plugins instanceof Array) {
				this.plugins.push(filterPlugin);
			}
		}

		// Init paging toolbar

		this.bbar = new Ext.PagingToolbar({
			store: this.store,
			pageSize: this.pageSize
		});

		this.bbar.on('render', function(cmp) {
			cmp.insertButton(0, new Ext.Toolbar.Fill());
		}, this, { single: true });

		mojo.EntityGrid.superclass.initComponent.call(this);
	},

	initStore: function() {
		this.store.comp = this;
		this.filterType = this.store.filterType;
		this.selectType = this.store.selectType;
		this.createType = this.store.createType;
		this.updateType = this.store.updateType;
		this.deleteType = this.store.deleteType;

		this.on('render', function() {
			this.store.load({
				params: { start: 0, limit: this.pageSize }
			});
		}, this, { single: true });
	},

	newRecord: function() {
		var id = --this.idcnt;
		return new this.store.record({ id: id }, id);
	},

	addRecord: function(record) {
		record.set('id', record.id);
		record.commit();
		this.store.add(record);
		record.setState(mojo.record.states.CREATE);
		return record;
	},

	// private
	addHandler: function() {
		var record = this.addRecord(this.newRecord());
		this.startEditing(this.store.indexOf(record), 1);
	},

	// private
	removeHandler: function() {
		var selectionModel = this.getSelectionModel();
		var records = selectionModel.getSelections();

		for (var x = 0; x < records.length; x++) {
			records[x].setState(mojo.record.states.DELETE);
		}
	},

	// private
	submitHandler: function() {
		var creates = [];
		var updates = [];
		var deletes = [];

		var records = this.store.getModifiedRecords();
		var states = mojo.record.states;

		for (var x = 0; x < records.length; x++) {
			var record = records[x];

			switch (record.getState()) {
				case states.CREATE: {
					creates.push(this.createQuery(record));
					break;
				}
				case states.UPDATE: {
					updates.push(this.updateQuery(record));
					break;
				}
				case states.DELETE: {
					if (record.get('id') > 0) {
						deletes.push(this.deleteQuery(record));
					}

					break;
				}
			}
		}

		this.submitAction.disable();

		this.store.persist(creates, updates, deletes, function() {
			this.store.commitChanges();
			this.store.reload();
		}.createDelegate(this));
	},

	// Store interface

	selectFilter: function() {
		return null;
	},

	selectQuery: function() {
		return new this.selectType();
	},

	createQuery: function(rec) {
		var query = new this.createType();
		query.entity = rec.getData();
		query.entity.id = null;
		return query;
	},

	updateQuery: function(rec) {
		var query = new this.updateType();
		query.entity = rec.getData();
		return query;
	},

	deleteQuery: function(rec) {
		var query = new this.deleteType();
		query.id = new Number(rec.get('id'));
		return query;
	},

	// Renderers

	stateRenderer: function(value, meta, record) {
		var states = mojo.record.states;

		switch (record.getState()) {
			case states.CREATE:
				meta.css = 'o-eg-rec-create';
				break;
			case states.UPDATE:
				meta.css = 'o-eg-rec-update';
				break;
			case states.DELETE:
				meta.css = 'o-eg-rec-delete';
				break;
			default:
				meta.css = 'o-eg-rec-normal';
		}

		return '';
	},

	idRenderer: function(value) {
		return typeof value == 'number' && value > 0 ? value : '';
	},

	objRenderer: function(path) {
		var parts = path.split('.');
		var renderer = function(value) {
			for (var x = 0; x < parts.length; x++) {
				if (!isObject(value)) {
					return '';
				}

				var part = parts[x];
				value = value[part];
			}

			return value;
		};

		renderer.path = path;
		return renderer;
	}
});
