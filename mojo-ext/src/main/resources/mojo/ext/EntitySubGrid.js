Ext.namespace('mojo');

mojo.EntitySubGrid = Ext.extend(mojo.EntityGrid, {

	disabled: true,

	linkProperty: 'masterId',

	initComponent: function() {
		this.on('beforerender', function() {
			if (this.ownerCt) {
				this.ownerCt.on('expand', function() {
					this.expandedOwner = true;
					this.updateStore();
				}, this);

				this.ownerCt.on('collapse', function() {
					this.expandedOwner = false;
				}, this);

				this.on('activate', function() {
					this.activated = true;
					this.updateStore();
				}, this);

				this.on('deactivate', function() {
					this.activated = false;
				}, this);
			}
		}, this, { single:true });

		mojo.EntitySubGrid.superclass.initComponent.call(this);
	},

	initStore: function() {
		this.store.comp = this;
		this.filterType = this.store.filterType;
		this.selectType = this.store.selectDetailType;
		this.createType = this.store.createDetailType;
		this.updateType = this.store.updateDetailType;
		this.deleteType = this.store.deleteType;

		this.parentGrid.getSelectionModel().on('selectionchange', function(selectionModel) {
			this.updateStore();

			if (this.disabled) {
				this.setDisabled(!selectionModel.hasSelection());
			}
		}, this);
	},

	updateStore: function() {
		if (!this.expandedOwner || !this.activated) return;

		if (!this.parentGrid.getSelectionModel().hasSelection()) {
			this.store.removeAll();
		}
		else {
			this.store.load({ params: { start: 0, limit: this.pageSize } });
		}
	},

	getMasterId: function() {
		var record = this.parentGrid.getSelectionModel().getSelected();
		var id = record.get('id');
		return new Number(id);
	},

	selectQuery: function() {
		var query = mojo.EntitySubGrid.superclass.selectQuery.call(this);
		query[this.linkProperty] = this.getMasterId();
		return query;
	},

	createQuery: function(entity) {
		var query = mojo.EntitySubGrid.superclass.createQuery.call(this, entity);
		query[this.linkProperty] = this.getMasterId();
		return query;
	},

	updateQuery: function(entity) {
		var query = mojo.EntitySubGrid.superclass.updateQuery.call(this, entity);
		query[this.linkProperty] = this.getMasterId();
		return query;
	}
});
