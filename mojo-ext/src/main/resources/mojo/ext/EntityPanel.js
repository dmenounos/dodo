Ext.namespace('mojo');

mojo.EntityPanel = Ext.extend(Ext.Panel, {

	layout: 'border',

	initComponent: function() {
		this.master.region = 'center';
		this.title = this.master.name;
		this.items = [ this.master ];

		if (this.details) {
			this.items.push(this.getDetailsPanel());
			var tbar = this.master.topToolbar;

			if (!tbar) {
				this.master.elements += ',tbar';
				this.master.topToolbar = new Ext.Toolbar();
				tbar = this.master.topToolbar;
			}

			tbar.add('-');

			for (var x = 0; x < this.details.length; x++) {
				tbar.add(this.createButton(x));
			}
		}

		mojo.EntityPanel.superclass.initComponent.call(this);
	},

	getDetailsPanel: function() {
		if (!this.detailsPanel) {
			this.detailsPanel = new Ext.Panel({
				region: 'east',
				split: true,
				collapsed: true,
				collapsible: true,
				collapseMode: 'mini',
				hideCollapseTool: true,
				border: false,
				layout: 'card',
				activeItem: 0,
				items: [ new Ext.BoxComponent() ]
			});
		}

		return this.detailsPanel;
	},

	createButton: function(x) {
		return new Ext.Button({
			text: this.details[x].name,
			iconCls: 'mc-table',
			enableToggle: true,
			toggleGroup: 'details-' + this.getId(),
			toggleHandler: function(btn, pressed) {
				var detailsPanel = this.getDetailsPanel();

				if (Ext.ButtonToggleMgr.getPressed(btn.toggleGroup)) {
					var detailPanel = this.details[x];
					var detailPanelId = detailPanel.getId();

					if (!detailsPanel.getComponent(detailPanelId)) {
						detailsPanel.add(detailPanel);
					}

					detailsPanel.getLayout().setActiveItem(detailPanelId);
					detailsPanel.setWidth(this.getWidth() / 2);
					detailsPanel.expand(true);
				} else {
					detailsPanel.collapse(true);
				}
			},
			scope: this
		});
	}
});
