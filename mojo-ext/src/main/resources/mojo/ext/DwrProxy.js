Ext.namespace('mojo');

mojo.DwrProxy = Ext.extend(Ext.data.DataProxy, {

	constructor: function(config) {
		mojo.DwrProxy.superclass.constructor.call(this, config);
	},

	// public, override
	doRequest: function(action, rs, params, reader, callback, scope, options) {
		var cb = this.createCallback.apply(this, arguments);

		var args = params.dwrArgs || [];
		args.push(!this.failure ? cb : { callback: cb, exceptionHandler: this.failure });

		var method = params.dwrMethod || this.api[action];
		method.apply(this, args);
	},

	// private
	createCallback: function(action, rs, params, reader, callback, scope, options) {
		var self = this;

		if (action == Ext.data.Api.actions.read) {
			return function(data) {
				self.onRead(data, action, reader, callback, scope, options);
			}
		}

		return Ext.emptyFn;
	},

	// public, override
	onRead: function(data, action, reader, callback, scope, options) {
		try {
			var result = reader.readRecords(data);
			callback.call(scope, result, options, true);
		}
		catch (e) {
			this.fireEvent('exception', this, 'response', action, options, data, e);
			callback.call(scope, null, options, false);
			return;
		}
	}
});
