Ext.namespace('mojo');

mojo.FormPlugin = Ext.extend(Object, {

	msgOk: 'Ok',
	msgClose: 'Close',
	msgDiscardChanges: 'Changes will be lost. Continue?',

	constructor: function(config) {
		Ext.apply(this, config);
	},

	init: function(grid) {
		if (grid.readOnly) {
			return;
		}

		this.grid = grid;
		this.grid.on('rowdblclick', this.editHandler, this);
		this.grid.addAction.setHandler(this.addHandler, this);
	},

	editHandler: function(grid, rowIndex, ev) {
		var record = grid.getStore().getAt(rowIndex);
		var win = this.createWindow();

		win.on('show', function(window) {
			var formPanel = window.items.itemAt(0);
			formPanel.getForm().loadRecord(record);
		});

		win.on('submit', function(window, formPanel) {
			formPanel.getForm().updateRecord(record);
			win.close();
		});

		win.show();
	},

	addHandler: function() {
		var win = this.createWindow();

		win.on('submit', function(window, formPanel) {
			var record = this.grid.newRecord();
			formPanel.getForm().updateRecord(record);
			formPanel.getForm().reset();
			this.grid.addRecord(record);
		}, this);

		win.show();
	},

	createWindow: function() {
		var winConfig = typeof this.winConfig == 'function'
		              ? this.winConfig()
		              : this.winConfig;

		var win = new Ext.Window(Ext.apply({
			width: 500,
			border: false,
			minimizable: true,
			items: this.createForm(),
			buttons: [
				new Ext.Button({
					text: this.msgOk,
					handler: function(btn) {
						var window = btn.ownerCt.ownerCt;
						var formPanel = window.items.itemAt(0);

						if (!formPanel.getForm().isValid()) {
							return;
						}

						window.fireEvent('submit', window, formPanel);
					},
					scope: this
				}),
				new Ext.Button({
					text: this.msgClose,
					handler: function(btn) {
						var window = btn.ownerCt.ownerCt;
						var formPanel = window.items.itemAt(0);

						if (formPanel.getForm().isDirty()) {
							Ext.Msg.confirm('', this.msgDiscardChanges, function(id) {
								if (id == 'yes') {
									window.close();
								}
							});
						} else {
							window.close();
						}
					},
					scope: this
				})
			],
			listeners: {
				show: {
					fn: function(cmp) {
						var fld = cmp.find('tabIndex', 1);

						if (fld.length > 0) {
							fld[0].focus();
						}
					},
					delay: 500
				},
				minimize: {
					fn: function(win) {
						if (!win.btn) {
							var bbar = this.grid.getBottomToolbar();

							if (!bbar.idx) {
								bbar.idx = 0;
							}

							win.btn = bbar.insert(bbar.idx++, new Ext.Button({
								text: win.title,
								handler: function() {
									win.show(this.el);
									bbar.remove(this);
									delete win.btn;
									bbar.idx--;
								}
							}));

							bbar.doLayout();
						}

						win.hide(win.btn.el);
					},
					scope: this
				}
			}
		}, winConfig));

		win.addEvents({
			submit: true
		});

		return win;
	},

	createForm: function() {
		var formConfig = typeof this.formConfig == 'function'
		               ? this.formConfig()
		               : this.formConfig;

		var form = new Ext.form.FormPanel(Ext.apply({
			trackResetOnLoad: true
		}, formConfig));

		return form;
	},

	getData: function(formPanel, types) {
		if (!types) types = {};

		var type = types[''];
		var root = type ? new type() : {};

		formPanel.cascade(function(item) {
			if (!(item instanceof Ext.form.Field) || item.disabled) {
				return;
			}

			var obj = root;
			var parts = item.name.split('.');
			var partName = '';

			for (var x = 0; x < parts.length; x++) {
				var part = parts[x];
				partName += (x > 0 ? '.': '') + part;

				if (x < parts.length - 1) {
					if (!obj[part]) {
						type = types[partName];
						obj[part] = type ? new type() : {};
					}

					obj = obj[part];
				} else {
					var value = item.getValue();

					if (value == '' && (item instanceof Ext.form.DateField)) {
						value = null;
					}

					obj[part] = value;
				}
			}
		});

		return root;
	},

	setData: function(formPanel, root) {
		var values = {};

		formPanel.cascade(function(item) {
			if (!(item instanceof Ext.form.Field) || item.disabled) {
				return;
			}

			var obj = root;
			var parts = item.name.split('.');

			for (var x = 0; x < parts.length; x++) {
				if (obj == null || typeof obj != 'object') {
					return;
				}

				obj = obj[parts[x]];
			}

			values[item.name] = obj;
		});

		formPanel.getForm().setValues(values);
	},

	getDirty: function(formPanel, types) {
		if (!types) types = {};

		var result = {};

		formPanel.cascade(function(item) {
			if ((item instanceof Ext.form.Field) && item.isDirty()) {
				for (var t in types) {
					if (t == '') {
						continue;
					} else if (item.name.indexOf(t) != -1) {
						result[t] = true;
						return;
					}
				}

				result[''] = true;
			}
		});

		return result;
	},

	chainHandlers: function(handlers, scope) {
		var chain = [];

		for (var x = handlers.length - 1; x >= 0; x--) {
			var hargs = [];

			if (x < handlers.length - 1) {
				hargs.push(chain[x + 1]);
			}

			chain[x] = handlers[x].createDelegate(scope, hargs, true);
		}

		return chain;
	}
});
