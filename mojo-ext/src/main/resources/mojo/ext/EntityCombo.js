Ext.namespace('mojo');

mojo.EntityCombo = Ext.extend(Ext.form.ComboBox, {

	valueField: 'id',
	triggerAction: 'all',
	typeAhead: true,
	minChars: 3,

	initComponent: function() {
		this.initStore();
		mojo.EntityCombo.superclass.initComponent.call(this);
	},

	initStore: function() {
		this.store.comp = this;
		this.filterType = this.store.filterType;
		this.selectType = this.store.selectType;
	},

	selectFilter: function() {
		var query = this.store.baseParams.query;

		if (query && this.mode == 'remote') {
			return Ext.apply(new this.filterType(), {
				properties: [ this.displayField ],
				value: query,
				mode: 1
			});
		}

		return null;
	},

	selectQuery: function() {
		return new this.selectType();
	},

	onRender: function(cnt, pos) {
		mojo.EntityCombo.superclass.onRender.call(this, cnt, pos);

		new Ext.KeyMap(this.el, {
			key: [ Ext.EventObject.BACKSPACE, Ext.EventObject.DELETE ],
			fn: function() {
				var rawValue = this.getRawValue();

				if (!rawValue || rawValue.length <= 1) {
					if (this.isExpanded()) {
						this.collapse();
					}

					this.clearValue();
				}
			},
			scope: this
		});
	},

	getValue: function() {
		var value = mojo.EntityCombo.superclass.getValue.call(this);

		if (isNumber(value)) {
			var record = this.store.getById(value);

			if (record) {
				return record.getData();
			}
		}

		return null;
	},

	setValue: function(value) {
		if (isObject(value)) {
			var record = this.store.getById(value.id);

			if (!record) {
				record = new this.store.record(value, value.id);
				this.store.add(record);
			}

			value = Number(value.id);
		}

		mojo.EntityCombo.superclass.setValue.call(this, value);
	}
});

Ext.ComponentMgr.registerType('entitycombo', mojo.EntityCombo);
