Ext.namespace('mojo');

mojo.XmlStore = Ext.extend(Ext.data.GroupingStore, {

	filterType       : mojo.xml.create('ByProperties'),
	selectType       : mojo.xml.create('Select'),
	selectDetailType : mojo.xml.create('SelectDetail'),
	createType       : mojo.xml.create('Insert'),
	createDetailType : mojo.xml.create('InsertDetail'),
	updateType       : mojo.xml.create('Update'),
	updateDetailType : mojo.xml.create('UpdateDetail'),
	deleteType       : mojo.xml.create('Delete'),
	batchType        : mojo.xml.create('Batch'),

	constructor: function(config) {
		mojo.XmlStore.superclass.constructor.call(this, Ext.apply({
			reader: new Ext.data.XmlReader({
				idProperty: 'id',
				totalProperty: 'total',
				record: config.record.prototype._type
			}, config.record),
			proxy: new Ext.data.HttpProxy({
				api: { read: config.controller + '/fetch.do' },
				failure: this.failureHandler
			}),
			remoteSort: true,
			pruneModifiedRecords: true
		}, config));

		this.proxy.on('beforeload', function(proxy, params) {
			var query = Ext.apply(this.comp.selectQuery(), {
				offset: params.start || 0,
				limit: params.limit || 0,
				order: params.order || null
			}, this.fetchDefaults);

			var fetchDefaults = this.fetchDefaults || {};
			var filters = fetchDefaults.filters || [];
			var filter = this.comp.selectFilter();
			var tmp = [];

			for (var x = 0; x < filters.length; x++) {
				tmp.push(filters[x]);
			}

			if (filter) {
				tmp.push(filter);
			}

			query.filters = tmp;
			params.xmlData = query.toXml();

			if (this.view) {
				params.view = this.view;
				proxy.setUrl(this.controller + '/fetch-view.do');
			}
		}, this);
	},

	persist: function(inserts, updates, deletes, callback) {
		var batch = new this.batchType({
			inserts: inserts,
			updates: updates,
			deletes: deletes
		});

		Ext.Ajax.request({
			url: this.controller + '/persist.do',
			xmlData: batch.toXml(),
			success: callback,
			failure: this.failureHandler
		});
	},

	failureHandler: function(resp, opt) {
		var popup = window.open('', 'error', 'width=500,height=250');
		popup.document.write(resp.responseText);
		popup.document.close();
	}
});
